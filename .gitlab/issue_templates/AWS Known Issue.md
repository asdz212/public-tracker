
<!--
Make the title descriptive.

This issue will be automatically tagged with ~"AWS Known Issue" and therefore appear in the list linked in the AWS SRE page.

Any of these communities might be subscribed to this label:

- GitLab Alliances for AWS.
- GitLab SREs running GitLab on AWS - this community should be able to easily find, interact and subscribe to known issues.
- Customers and GitLab Channel Partners - what is the status of known issues and what is a complete list of known issues for a platform partner.
- AWS Partner Manager Team - to help keep focus and easy reference to top issues.
- AWS Product Teams - drive issues and us having a listing will help AWS product teams understand when something is important enough to be handled at a partnership level.
- GitLab Product Teams - by enhancing visibility of these issues at the partner level we can be transparent about what issues are important to and being managed through our partnership.

-->

## Related Technical Issues

If there are additional related issues they will be added to the section Linked issues below.

## Getting Updates

You can subscribe to this issue to follow progress and resolution status.

/label ~"AWS Known Issue"

GO TO THE TOP of this issue template to complete the entire form! Issue templates frequently place you at the bottom when loaded, but there is more information required above.
